# python:3.5-slim has been used because it is small and works for this app. If
# something begins to work wrong you can use python:3.5 or ubuntu:16.04 images
# (ubuntu:16.04 is smaller than python:3.5).
# More about this image at https://hub.docker.com/_/python/
FROM python:3.5-slim

# If new python packages are needed, add them to docker_files/pip_packages.txt
# one per line.
COPY docker_files/pip_packages.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

# Copies all the files in this directory into /code in the Docker image.
ADD ./ /code
WORKDIR /code

# Use a non root user to run the app.
RUN adduser --uid 1000 --disabled-password --gecos '' nruser && \
    chown -R nruser:nruser /code
USER nruser

# /code/main.py is the entry point of the app.
CMD ["python", "./src/main.py"]
