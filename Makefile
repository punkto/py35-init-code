image_name = demo:main
image_hash = $(shell docker images -q $(image_name) 2> /dev/null)
work_dir = $(shell pwd)

build: Dockerfile
	@if test "$(image_hash)" = ""  ; then \
		docker build --tag $(image_name) . ; \
	else \
		echo "Image already exists" ; \
	fi

run: build
	docker run -ti --rm $(image_name)

runSync: build
	docker run -ti --rm --volume=$(work_dir)/:/code:ro $(image_name)

runTerm: build
	docker run -ti --rm --volume=$(work_dir)/:/code:ro $(image_name) bash

runPython: build
	docker run -ti --rm --volume=$(work_dir)/:/code:ro $(image_name) python


tests: build
	docker run -ti --rm --volume=$(work_dir)/:/code:ro $(image_name) python3 -m unittest discover -s tests -v


clean:
	@if test "$(image_hash)" = ""  ; then \
		echo "Image does not exist" ; \
	else \
		docker rmi $(image_name) ; \
	fi

dockerRemoveContainers:
	docker rm $(docker ps -a -q)

dockerRemoveUntaggedImages:
	docker rmi $(docker images -a | grep "^<none>" | awk '{print $3}')

