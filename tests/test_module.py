import unittest
from src import module


class TestModule(unittest.TestCase):

    def test_whatever(self):
        self.assertEqual(module.whatever(), 'whatever')

if __name__ == '__main__':
    unittest.main()
